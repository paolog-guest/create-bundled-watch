#!/usr/bin/env python3
# coding=utf-8
#
# generate debian/watch required for https://wiki.debian.org/Javascript/GroupSourcesTutorial
# modify the data dictionary as required and invoke with:
#   ./deb_create_bundled_watch.py
#
# Copyright (C) 2018 Paolo Greppi
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from jinja2 import Template

template = Template("""version=4
  
opts="searchmode=plain,pgpmode=none" \\
 https://registry.npmjs.org/{{ main_module }} \\
 https://registry.npmjs.org/{{ main_module }}/-/{{ main_module }}-(\\d[\\d\\.]*)@ARCHIVE_EXT@ group
{% for sub_module1 in group %}
opts="searchmode=plain,pgpmode=none,component={{ sub_module1 }}" \\
 https://registry.npmjs.org/{{ sub_module1 }} \\
 https://registry.npmjs.org/{{ sub_module1 }}/-/{{ sub_module1 }}-(\\d[\\d\\.]*)@ARCHIVE_EXT@ group
{% endfor %}{% for sub_module1 in ignore %}
opts="searchmode=plain,pgpmode=none,component={{ sub_module1 }}" \\
 https://registry.npmjs.org/{{ sub_module1 }} \\
 https://registry.npmjs.org/{{ sub_module1 }}/-/{{ sub_module1 }}-(\\d[\\d\\.]*)@ARCHIVE_EXT@ ignore
{% endfor %}
""")

# example from wiki.d.o
data = {
  'main_module': 'test',
  'group': sorted(['ta', 'tb']),
  'ignore': []
}

# for https://salsa.debian.org/js-team/node-yarnpkg
data = {
  'main_module': 'yarnpkg',
  'group': [],
  'ignore': sorted(['babel-plugin-transform-inline-imports-commonjs', 'v8-compile-cache', 'dnscache', 'tar-fs', 'normalize-url'])
}
print(template.render(data))

# for npm
data = {
  'main_module': 'npm',
  'group': [],
  'ignore': sorted([
    'ansicolors',
    'bin-links',
    'cli-table2',
    'columnify',
    'debuglog',
    'dezalgo',
    'find-npm-prefix',
    'gentle-fs',
    'init-package-json',
    'is-cidr',
    'json-parse-better-errors',
    'libcipm',
    'lodash._baseindexof',
    'lodash._baseuniq',
    'lodash._bindcallback',
    'lodash._cacheindexof',
    'lodash.clonedeep',
    'lodash._createcache',
    'lodash._getnative',
    'lodash.restparam',
    'lodash.union',
    'lodash.uniq',
    'lodash.without',
    'meant',
    'npm-cache-filename',
    'npm-install-checks',
    'npm-lifecycle',
    'npm-packlist',
    'npm-profile',
    'npm-registry-client',
    'npm-user-validate',
    'pacote',
    'qrcode-terminal',
    'query-string',
    'read-cmd-shim',
    'readdir-scoped-modules',
    'read-installed',
    'read-package-tree',
    'sorted-union-stream',
    'uid-number',
    'umask',
    'update-notifier',
    'worker-farm'])
}
